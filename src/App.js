import React, { useState, useRef, useEffect } from "react";
import validator from "validator";
//import Modal from "react-modal";
import moment from "moment";
import "react-datepicker/dist/react-datepicker.css";

const customStyles = {
  textAlign: "center",
  display: "none",
  padding: 30,
  margin: "auto",
  marginTop: "130px",
  background: "yellow",
};

export default function App() {
  const [open, setOpen] = React.useState(false);
  const [enteredEmail, setEnteredemail] = useState("");
  const [enteredNumber, setEnterednumber] = useState("");
  const [enteredDate, setEntereddate] = useState("");
  var modalcontent = document.getElementsByClassName("modal-content");
  var form = document.getElementsByClassName("form");
  var form1 = document.getElementsByClassName("form");
  // When the user clicks anywhere outside of the modal, close it
  const newRef = useRef(null);
  function myFunction() {
    var x = document.getElementById("form");
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }

  function emailChangeHandler(event) {
    setEnteredemail(event.target.value);
  }
  function numberChangeHandler(event) {
    setEnterednumber(event.target.value);
  }
  function dateChangeHandler(event) {
    setEntereddate(event.target.value);
  }
  function clearFields(event) {
    Array.from(event.target).forEach((e) => (e.value = ""));
  }
  function validate(date) {
    var YearsAgo = moment().subtract(18, "years");
    var birthday = moment(date);

    if (YearsAgo.isAfter(birthday)) {
      alert("Invalid date of birth. Date of birth can not be in the future");
    }
  }

  function handleSubmit(event) {
    event.preventDefault();

    if (!validator.isEmail(enteredEmail)) {
      alert("Invalid email. Please check your email address.");
    }
    if (enteredNumber.length != 10) {
      alert("Invalid phone number. Please enter a 10-digit phone number.");
    }
    validate(enteredDate);

    clearFields(event);
  }
  useEffect(() => {
    document.addEventListener("mousedown", handleOutsideClick);
    return () => {
      document.removeEventListener("mousedown", handleOutsideClick);
    };
  });
  const handleOutsideClick = (e) => {
    if (newRef.current && !newRef.current.contains(e.target)) {
      var x = document.getElementById("form");
      if (x.style.display === "block") {
        x.style.display = "none";
      }
    }
  };

  return (
    <>
      <div
        className="modal"
        style={{
          textAlign: "center",
          display: "block",
          padding: 30,
          margin: "auto",
        }}
      >
        <div className="modal-content">
          <h1>User Details Model</h1>
          <button onClick={myFunction}>Open Form</button>
          <form
            ref={newRef}
            id="form"
            style={customStyles}
            onSubmit={(event) => handleSubmit(event)}
          >
            <label>Username:</label>
            <br />
            <input id="username" type="text" required />
            <br />
            <label>Email Address:</label>
            <br />
            <input
              value={enteredEmail}
              onChange={emailChangeHandler}
              id="email"
              type="email"
              required
            />
            <br />
            <label>Phone Number:</label>
            <br />
            <input
              id="phone"
              type="number"
              value={enteredNumber}
              onChange={numberChangeHandler}
              minlength="10"
              maxlength="10"
              required
            />
            <br />
            <label>Date of Birth:</label>
            <br />
            <input
              id="dob"
              value={enteredDate}
              onChange={dateChangeHandler}
              type="date"
              required
            />
            <br />
            <button className="submit-button" type="submit">
              Submit
            </button>
          </form>
        </div>
      </div>
    </>
  );
}
